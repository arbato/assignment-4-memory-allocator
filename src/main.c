#include "mem_internals.h"
#include "mem.h"
#include <assert.h>


void allocation() {
    void* test_heap = heap_init(0);
    assert(test_heap != NULL);
    debug_heap(stdout, HEAP_START);
    void* test_block = _malloc(1024);
    assert(test_block != NULL);
    debug_heap(stdout, HEAP_START);
    _free(test_block);
    heap_term();
}
void free_one_block() {
    void* test_heap = heap_init(0);
    assert(test_heap != NULL);
    void* test_block1 = _malloc(1024);
    assert(test_block1 != NULL);
    void* test_block2 = _malloc(1024);
    assert(test_block2 != NULL);
    debug_heap(stdout, HEAP_START);
    _free(test_block2);
    debug_heap(stdout, HEAP_START);
    struct block_header* test_block_header1 = block_get_header(test_block1);
    struct block_header* test_block_header2 = block_get_header(test_block2);
    assert(test_block_header1->next == test_block_header2);
    assert(test_block_header2->is_free);
    _free(test_block1);
    heap_term();
}
void free_two_blocks() {
    void* test_heap = heap_init(0);
    assert(test_heap != NULL);
    void* test_block1 = _malloc(1024);
    assert(test_block1 != NULL);
    void* test_block2 = _malloc(1024);
    assert(test_block2 != NULL);
    void* test_block3 = _malloc(1024);
    assert(test_block3 != NULL);
    debug_heap(stdout, HEAP_START);
    _free(test_block3);
    debug_heap(stdout, HEAP_START);
    struct block_header* test_block_header1 = block_get_header(test_block1);
    struct block_header* test_block_header2 = block_get_header(test_block2);
    struct block_header* test_block_header3 = block_get_header(test_block3);
    assert(test_block_header2->next == test_block_header3);
    assert(test_block_header3->is_free);
    _free(test_block2);
    debug_heap(stdout, HEAP_START);
    assert(test_block_header1->next == test_block_header2);
    assert(test_block_header2->is_free);
    _free(test_block1);
    heap_term();
}
void extends_region() {
    void* test_heap = heap_init(0);
    assert(test_heap != NULL);
    void* test_block1 = _malloc(4096);
    assert(test_block1 != NULL);
    debug_heap(stdout, HEAP_START);
    void* test_block2 = _malloc(4096*3);
    assert(test_block2 != NULL);
    debug_heap(stdout, HEAP_START);
    _free(test_block2);
    _free(test_block1);
    heap_term();
}

static void extends_region_in_different_location(){
    void* heap = heap_init(REGION_MIN_SIZE);

    assert(heap != NULL);

    debug_heap(stdout, heap);
    void* block1 = _malloc(REGION_MIN_SIZE);
    assert(block1 != NULL);

    struct block_header* header1 = block_get_header(block1);
    assert(header1 != NULL);

    debug_heap(stdout, heap);

    void* addr = header1->contents + header1->capacity.bytes;
    void* new_region = mmap(addr, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED , -1, 0 );

    void* block2 = _malloc(REGION_MIN_SIZE);
    assert(block2 != NULL);

    struct block_header* header2 = block_get_header(block2);
    assert(header2 != NULL);


    munmap(new_region, size_from_capacity((block_capacity){.bytes = REGION_MIN_SIZE}).bytes);
    size_t capacity = ((struct block_header*) heap)->capacity.bytes + header1->capacity.bytes + header2->capacity.bytes;
    munmap(heap, capacity);
}



int main() {
    allocation();
    free_one_block();
    free_two_blocks();
    extends_region();
    extends_region_in_different_location();
    return 0;
}